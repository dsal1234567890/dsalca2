#include "Admin.h"

Admin::~Admin()
{
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	while (itorForGame != endForGame)
	{
		delete *itorForGame;
		itorForGame++;
	}
	list<Rating*>::iterator itorForRating = listOfRating.begin();
	list<Rating*>::iterator endForRating = listOfRating.end();
	while (itorForRating != endForRating)
	{
		delete *itorForRating;
		itorForRating++;
	}
}

void Admin::AddGame()
{
	string name,publisher,developer,description,genre;
	int cost,id,count = 0;
	char str[500];
	bool checkID = false;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	cout << "Please enter the name of the game" << endl;
	cin >> name;
	cout << "Please enter the price of the game" << endl;
	cin >> cost;
	cout << "Please enter the name of the publisher of the game" << endl;
	cin >> publisher;
	cout << "Please enter the name of the developer of the game" << endl;
	cin >> developer;
	cout << "Please enter the description of the game and end with full stop" << endl;
	cin.getline(str, 500,'.');
	description  = str ;
	cout << description << endl;
	cout << "Please enter the genre of the game" << endl;
	cin >> genre;
	cout << "Please enter a unique game ID" << endl;
	id = listOfGame.size()+1;
	list<int>ratingList;
	ratingList.push_back(1);
	ratingList.push_back(1);
	ratingList.push_back(1);
	ratingList.push_back(1);
	ratingList.push_back(1);
	listOfGame.push_back(new Game(name,cost,publisher,developer,description,genre,id,ratingList));
	cout << "You have successfully add this item" << endl;
}

void Admin::EditGame()
{
	cout << "Please enter the game ID of the game you wish to edit" << endl;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	int id;
	cin >> id;
	int count = 0;
	while(itorForGame != endForGame)
	{
		if((*itorForGame)->GetID() == id)
		{
			count++;
			break;
		}
		itorForGame++;
	}
	if(count == 0)
	{
		cout << "Unable to edit game due to wrong game ID" << endl;
		cout << "===============================================================================" << endl << endl << endl;
	}
	else
	{
		bool exitCalled = false;
		cout << "Please choose which part to edit : 'name' , 'cost' , 'develper' , 'publisher' , 'genre' or 'description'" << endl;
		string input;
		cin >> input;
		while(exitCalled == false)
		{
			if ( "name" == input)
			{
				string name;
				cout << "Please enter the changes you want" << endl;
				cin >> name;
				(*itorForGame)->SetName(name);
				cout << "You have succesfully edited the name details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else if ("cost" == input)
			{
				int cost;
				cout << "Please enter the changes you want" << endl;
				cin >> cost;
				(*itorForGame)->SetCost(cost);
				cout << "You have succesfully edited the cost details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else if ("developer" == input)
			{
				string developer;
				cout << "Please enter the changes you want" << endl;
				cin >> developer;
				(*itorForGame)->SetDeveloper(developer);
				cout << "You have succesfully edited the developer details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else if ("publisher" == input)
			{
				string publisher;
				cout << "Please enter the changes you want" << endl;
				cin >> publisher;
				(*itorForGame)->SetPublisher(publisher);
				cout << "You have succesfully edited the publisher details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else if ("genre" == input)
			{
				string genre;
				cout << "Please enter the changes you want" << endl;
				cin >> genre;
				(*itorForGame)->SetGenre(genre);
				cout << "You have succesfully edited the genre details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else if ("description" == input)
			{
				string description;
				cout << "Please enter the changes you want" << endl;
				cin >> description;
				(*itorForGame)->SetDescription(description);
				cout << "You have succesfully edited the description details" << endl;
				cout << "Thus exitting edit mode." << endl;
				exitCalled = true;
			}
			else
			{
				cout << "Please enter a valid option" << endl;
				input = "";
				cin >> input;
			}
		}
	}
}

void Admin::RemoveGame()
{
	int id;
	cout  << "Please Enter the game ID you wish to remove" << endl;
	cin >> id;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	int count = 0;
	while(itorForGame != endForGame)
	{
		if((*itorForGame)->GetID() == id)
		{
			count++;
			break;
		}
		itorForGame++;
	}
	if(count == 0)
	{
		cout << "Unable to remove due to wrong game ID" << endl;
		cout << "===============================================================================" << endl << endl << endl;
	}
	else
	{
		listOfGame.erase(itorForGame);//havent test
		cout << "The game has been successfully removed from the store" << endl;
	}
}

void Admin::ExitAdmin()
{
	listOfGame.sort(compare_id_asc);
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	fstream outputGame;
	outputGame.open("Product.txt");
	outputGame.close();
	outputGame.open("Product.txt",std::fstream::out | std::fstream::trunc);
	while ( itorForGame != endForGame )
	{
		outputGame << (*itorForGame)->GetName() << "/ ";
		outputGame << (*itorForGame)->GetCost() << " / ";
		outputGame << (*itorForGame)->GetDeveloper() << " / ";
		outputGame << (*itorForGame)->GetPublisher() << " / ";
		outputGame << (*itorForGame)->GetDescription() << " / ";
		outputGame << (*itorForGame)->GetGenre() << " / ";
		outputGame << (*itorForGame)->GetID() << endl;
	    itorForGame++;
	}
	outputGame.close();
	fstream outputRating;
	list<Rating*>::iterator itorForRating = listOfRating.begin();
	list<Rating*>::iterator endForRating = listOfRating.end();
	outputRating.open("Rating.txt",fstream::in);
	outputRating.close();
	outputRating.open("Rating.txt",std::fstream::out | std::fstream::trunc);
	while ( itorForRating != endForRating )
	{
		outputRating << **itorForRating <<"\n";
		itorForRating++;
	}
	outputRating.close();
}

void Admin::AdminAction()
{
	string function = "";
	cin >> function;
	while(function != "0"){
		if("add" == function)
		{
			AddGame();
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			cin >> function;
			cout << endl;
		} else if ("edit" == function)
		{
			EditGame();
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			cin >> function;
			cout << endl;
		} else if("remove" == function)
		{
			RemoveGame();
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			cin >> function;
			cout << endl;
		} else if ("gameinfo" == function)
		{
			User::PrintGameInfo();
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			cin >> function;
			cout << endl;
		} else if ("sort" == function)
		{
			string input;
			cout << "What would you like to sort by: 'costasc','costdesc','ratingasc','ratingdesc','idasc'" << endl;
			cin >> input;
			cout << endl;
			User::PrintGameTitle(input);
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			cin >> function;
			cout << endl;
		} else
		{
			cout << "===============================================================================" << endl << endl << endl;
			cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl;
			function = "";
			cin >> function;
			cout << endl;
		}
	}
}
void Admin::PrintGameTitle()
{
	User::PrintGameTitle();
	cout << "What do you want to do? Add? Edit? Remove ames? View game info? Sort?" << endl << endl;
	cout << "Please type an action - - - 'add' 'edit' 'remove' 'gameinfo' 'sort'" << endl << endl;
	cout << "You can type '0' to exit the Admin mode" << endl << endl;
}