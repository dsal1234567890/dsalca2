#ifndef Admin_H
#define Admin_H
#include "User.h"
#include <iostream>
#include <string>
#include <list>
using namespace std;

class Admin : public User
{
public:
	
	//Pre-Condition: When the admin exits
	//Post-Condition: Destroys admin

	~Admin();

	//Pre-Condition: When the admin wants to add a game to the store
	//Post-Condition: Game is added into the store

	void AddGame();

	//Pre-Condition: When the admin wants to remove a game from the store
	//Post-Condition: Game is removed from the store

	void RemoveGame();

	//Pre-Condition: When the admin wants to edit a specific part of a game
	//Post-Condition: Changes are made to the specific part of a game

	void EditGame();

	//Pre-Condition: When the admin exits
	//Post-Condition: Saves the changes made in the gamelist and ratinglist

	void ExitAdmin();

	//Pre-Condition: When the admin is initialised
	//Post-Condition: Display actions for admin to use

	void AdminAction();

	//Pre-Condition: When the admin wants to see the list of games
	//Post-Condition: List of games in admin view

	virtual void PrintGameTitle();

};
#endif