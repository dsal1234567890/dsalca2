#include "Game.h"
Game::Game()
{
}
Game::Game(string name1, int cost1,string developer1, string publisher1, string description1,string genre1, int id1, list<int> list1)
	: name(name1), cost(cost1), developer(developer1), publisher(publisher1), description(description1), genre(genre1), id(id1), ratingList(list1)
{
	list<int>::iterator itor = ratingList.begin();
	list<int>::iterator end = ratingList.end();
	int one,two,three,four,five,count = 0;
	while(itor!=end)
	{
		count++;
		if(count == 1)
		{
			one = *itor;
		}
		else if(count == 2)
		{
			two = *itor;
		}
		else if(count == 3)
		{
			three = *itor;
		}
		else if(count == 4)
		{
			four = *itor;
		}
		else if(count == 5)
		{
			five = *itor;
		}
		itor++;
	}
	//avgRatings = (((one*0.2)+(two*0.4)+(three*0.6)+(four*0.8)+five)/(one+two+three+four+five));
	avgRatings = (((one*0.2)+(two*0.4)+(three*0.6)+(four*0.8)+(five))/(one+two+three+four+five)) * 5;
	avgRatings = floor(avgRatings);
}
ostream& operator <<(ostream &out, Game game){
	out <<"Game ID:\t"<< game.id << "\n" << "Game Title:\t" << game.name << "\n"<<"Game Price:\t$" << game.cost << "\n"<< "Rating:\t" <<  game.avgRatings << endl;
	out << "===============================================================================";
	return out;
}
istream& operator >>(istream &in, Game &game){
	
	
	if(std::getline(in,game.name,'/'))
	{
		in >> game.cost;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'/');
		in >> game.developer;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'/');
		in >> game.publisher;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'/');
		char str[500];
		in.get(str,std::numeric_limits<std::streamsize>::max(),'/');
		game.description = str;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'/');
		in >> game.genre;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'/');
		in >> game.id;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
	}
	else
	{
		game.name = "*****";
	}

	return in;
}
string Game::GetName()const{
	return name;
}
int Game::GetID()const{
	return id;
}
int Game::GetCost()const{
	return cost;
}
string Game::GetDeveloper()const{
	return developer;
}
string Game::GetPublisher()const{
	return publisher;
}
string Game::GetDescription()const{
	return description;
}
string Game::GetGenre()const{
	return genre;
}
double Game::GetRating()const
{
	return avgRatings;
}
void Game::SetName( string name1){
	name = name1;
}
void Game::SetCost( int cost1 ){
	cost = cost1;
}
void Game::SetDeveloper( string developer1 ){
	developer = developer1;
}
void Game::SetPublisher( string publisher1 ){
	publisher = publisher1;
}
void Game::SetDescription( string description1 ){
	description = description1;
}
void Game::SetGenre( string genre1 ){
	genre = genre1;
}
void Game::SetID(int id1){
	id = id1;
}
void Game::SetRating(int one,int two,int three,int four,int five)
{
	list<int>::iterator itor = ratingList.begin();
	list<int>::iterator end = ratingList.end();
	*itor += one;
	itor++;
	*itor += two;
	itor++;
	*itor += three;
	itor++;
	*itor += four;
	itor++;
	*itor += five;
	int oneStar, twoStar, threeStar, fourStar, fiveStar;
	itor = ratingList.begin();
	oneStar = *itor;
	itor++;
	twoStar = *itor;
	itor++;
	threeStar = *itor;
	itor++;
	fourStar = *itor;
	itor++;
	fiveStar = *itor;
	avgRatings = (((oneStar*0.2)+(twoStar*0.4)+(threeStar*0.6)+(fourStar*0.8)+(fiveStar))/(oneStar+twoStar+threeStar+fourStar+fiveStar)) * 5;
	avgRatings = floor(avgRatings * 100 + 0.5)/100;
}

void Game::SetRating(list<int> &rating)
{
	ratingList = rating;
	list<int>::iterator itor = ratingList.begin();
	list<int>::iterator end = ratingList.end();
	int one,two,three,four,five;
	one = *itor;
	itor++;
	two = *itor;
	itor++;
	three = *itor;
	itor++;
	four = *itor;
	itor++;
	five = *itor;
	avgRatings = (((one*0.2)+(two*0.4)+(three*0.6)+(four*0.8)+(five))/(one+two+three+four+five)) * 5;
	avgRatings = floor(avgRatings * 100 + 0.5)/100;
}