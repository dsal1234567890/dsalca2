#ifndef Game_H
#define Game_H
#include <iostream>
#include <string>
#include <list>
using namespace std;

class Game
{
public:
	Game();
	Game(string , int,string,string,string,string,int,list<int>);
	string GetName()const;
	int GetCost()const;
	int GetID()const;
	string GetDeveloper()const;
	string GetPublisher()const;
	string GetDescription()const;
	string GetGenre()const;
	double GetRating()const;
	//void SetAvgRating();
	friend ostream& operator << (ostream &out, Game game);
	friend istream& operator >> (istream &in, Game &game);
	void SetName( string);
	void SetCost( int);
	void SetDeveloper(string);
	void SetPublisher(string);
	void SetDescription( string);
	void SetGenre(string);
	void SetID(int);
	void SetRating(int,int,int,int,int);
	void SetRating(list<int>&);
private:
	string name;
	int cost;
	string developer;
	string publisher;
	string description;
	string genre;
	int id;
	list<int>ratingList;
	double avgRatings;
};
#endif