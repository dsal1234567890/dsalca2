#include "Game.h"
#include "Shopper.h"
#include "Admin.h"
#include "User.h"
#include "Rating.h"
#include <iostream>
#include <list>
#include <fstream>
#include <string>

void main(){
	cout << "Welcome to Galore game shop!" << endl;
	string input;
	while (input != "0"){
		cout << "Please select a mode 'admin' 'shopper' or '0' to exit: " << endl;
		cin >> input;
		cout << endl;
		if (input == "admin")
		{
			Admin *admin = new Admin();
			admin ->PrintGameTitle();
			admin ->AdminAction();
			admin ->ExitAdmin();
			delete admin;

		}
		else if (input == "shopper")
		{
			Shopper *shopper = new Shopper();
			shopper ->ShopperAction();
			delete shopper;
		} 
		else if ( input == "0") 
		{
		}
		else 
		{
			cout << "Invalid input!! " << endl << endl;
			input = "";
		}
	}
	cout << "Thanks for visiting Galore!" << endl;
}