#include "Rating.h"

Rating::Rating()
{
}
list<int> Rating::GetRatingList()
{
	return ratingList;
}
int Rating::GetID()
{
	return gameID;
}
istream& operator >>(istream &in, Rating &rating)
{
	int one = 0;
	int two = 0;
	int three = 0;
	int four = 0;
	int five = 0;
	if(in.good())
	{
		in >> rating.gameID >> one >> two >> three>>four>>five;
		in.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
		rating.ratingList.push_back(one);
		rating.ratingList.push_back(two);
		rating.ratingList.push_back(three);
		rating.ratingList.push_back(four);
		rating.ratingList.push_back(five);
	}
	else{
		rating.gameID = -999;
	}
	return in;
}
ostream& operator <<(ostream &out, Rating &rating)
{
	list<int>::iterator itor = rating.ratingList.begin();
	list<int>::iterator end = rating.ratingList.end();
	int one,two,three,four,five;
	one = *itor;
	itor++;
	two = *itor;
	itor++;
	three = *itor;
	itor++;
	four = *itor;
	itor++;
	five = *itor;
	itor++;
	out << rating.gameID << " "  << one << " "  << two <<  " "  << three <<  " " <<  four <<  " "   << five;
	return out;
}