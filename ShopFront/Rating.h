#ifndef Rating_H
#define Rating_H
#include <iostream>
#include <list>
using namespace std;
class Rating
{
public:
	Rating();

	//Pre-Condition: When the user wants to load ratings from file
	//Post-Condition: Ratings for each game are loaded

	friend istream& operator >> ( istream &in, Rating &rating);

	//Pre-Condition: When the user wants to save ratings to file
	//Post-Condition: Ratings for each game are saved

	friend ostream& operator << ( ostream &out , Rating &rating);

	//Pre-Condition: When the user wants to rate a game
	//Post-Condition: Ratings are appended to the game

	void setRatings(int,int,int,int,int);

	//Pre-Condition: When the user wants to see the list of ratings
	//Post-Condition: List of ratings are returned

	list<int> GetRatingList();

	//Pre-Condition: When the user wants to match the ratings to the game
	//Post-Condition: Returns the game ID

	int GetID();

protected:
	int gameID;
	list<int> ratingList;
};
#endif