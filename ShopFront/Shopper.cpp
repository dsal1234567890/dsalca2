#include "Shopper.h"
Shopper::~Shopper()
{
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	while (itorForGame != endForGame)
	{
		delete *itorForGame;
		itorForGame++;
	}
	list<Rating*>::iterator itorForRating = listOfRating.begin();
	list<Rating*>::iterator endForRating = listOfRating.end();
	while (itorForRating != endForRating)
	{
		delete *itorForRating;
		itorForRating++;
	}
	list<Game*>::iterator itorForCart = shoppingCart.begin();
	list<Game*>::iterator endForCart = shoppingCart.end();
	while (itorForCart != endForCart)
	{
		delete *itorForCart;
		itorForCart++;
	}

}
void Shopper::ShopperAction()
{
	PrintGameTitle();
	string function = "";
	while(function != "0"){
		cout << "===============================================================================" << endl << endl << endl;
		cout << "Please type an action 'addcart' 'filter' 'viewcart' 'gameinfo' 'rategame' 'sort' 'removecart' or '0' to exit" << endl;
		cin >> function;
		cout << "===============================================================================" << endl << endl << endl;
		if("filter" == function)
		{
			FilterBrowse();
		}
		else if ("addcart" == function)
		{
			AddCart();
		}

		else if ("viewcart" == function)
		{
			ViewCart();
		
		} else if ("removecart" == function)
		{
			RemoveCart();
		}
		else if ("gameinfo" == function)
		{
			User::PrintGameInfo();
		} 
		else if ("rategame" == function)
		{
		    GameRating();
		} 
		else if ("sort" == function)
		{
			string input;
			cout << "What would you like to sort by: 'costasc','costdesc','ratingasc','ratingdesc','idasc'" << endl;
			cin >> input;
			PrintGameTitle(input);
		} 
		else if ("0" == function)
		{
		}
		else
		{
			cout << "===============================================================================" << endl << endl << endl;
			cout << "Please type an action 'addcart' 'filter' or '0' to exit" << endl << endl;
			cout << "'addcart' to add product to your shopping cart" << endl << endl;
			cout << "'removecart' to remove product from your shopping cart" << endl << endl;
			cout << "'filter' to browse by filter" << endl << endl;
			cout << "'gameinfo' to view info of a game" << endl << endl;
			cout << "'rategame' to rate game" << endl << endl;
			cout << "'sort' to sort game" << endl << endl;
			function = "";
			cout << "===============================================================================" << endl << endl << endl;
		}
	} 
	if(!(shoppingCart.empty()))
	{
		CheckOut();
	}
	fstream outputRating;
	list<Rating*>::iterator itorForRating = listOfRating.begin();
	list<Rating*>::iterator endForRating = listOfRating.end();
	outputRating.open("Rating.txt",fstream::in);
	outputRating.close();
	outputRating.open("Rating.txt",std::fstream::out | std::fstream::trunc);
	while ( itorForRating != endForRating )
	{
		outputRating << **itorForRating <<"\n";
		itorForRating++;
	}
	outputRating.close();
}

void Shopper::FilterBrowse()
{
	bool exitCalled = false;
	string input;
	cout << "===============================================================================" << endl << endl << endl;
	cout << "What would you like to filter by: 'developer','publisher','genre'" << endl;
	cin >> input;
	cout << "===============================================================================" << endl << endl << endl;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	while(exitCalled == false){

		if(input == "developer")
		{
			string developer;
			int count = 0;
			cout << "Please enter a specific developer: " << endl;
			cin >> developer; 
			while (itorForGame != endForGame)
			{
				if((*itorForGame)->GetDeveloper() == developer)
				{
					cout << **itorForGame << endl;
					count++;
				}
				itorForGame++;
			}
			if( count == 0 )
				cout << "Unable to find that developer name"<<endl;
			exitCalled = true;
		} 
		else if(input == "publisher")
		{
			string publisher;
			int count = 0;
			cout << "Please enter a specific publisher: " << endl;
			cin >> publisher; 
			while (itorForGame != endForGame)
			{
				if((*itorForGame)->GetPublisher() == publisher)
				{
					cout << **itorForGame << endl;
					count++;
				}
				itorForGame++;
			}
			if( count == 0)
				cout << "Unable to find that publisher name"<<endl;
			exitCalled = true;
		}
		else if(input == "genre")
		{
			string genre;
			int count = 0;
			cout << "Please enter a specific genre: " << endl;
			cin >> genre; 
			while (itorForGame != endForGame)
			{
				if((*itorForGame)->GetGenre() == genre)
				{
					cout << **itorForGame << endl;
					count++;
				}
				itorForGame++;
			}
			if( count == 0)
				cout << "Unable to find any game relating to that genre"<<endl;
			exitCalled = true;
		} 
		else
		{
			cout << "===============================================================================" << endl << endl << endl;
			cout << "Please enter a valid filter option" << endl;
			input = "";
			cin >> input;
			cout << "===============================================================================" << endl << endl << endl;
		}
	}
}

void Shopper::AddCart()
{
	int id;
	cout << "===============================================================================" << endl << endl << endl;
	cout<< "Please select the Game ID of the product you wish to add to your shopping card " << endl;
	cin >> id;
	cout << "===============================================================================" << endl << endl << endl;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	int count = 0;
	while(itorForGame!=endForGame)
	{
		if((*itorForGame)->GetID() == id)
		{
			count++;
			break;
		}
		itorForGame++;
	}
	if(count == 0)
		cout << "Unable to add to cart due to wrong game ID" << endl;
	else{
		Game *game = new Game();
		game = *itorForGame;
		shoppingCart.push_back(game);
		cout << "The game has been successfully added to your shopping cart" << endl;
		cout << "===============================================================================" << endl << endl << endl;
	}
}

void Shopper::RemoveCart()
{
	int id;
	cout << "===============================================================================" << endl << endl << endl;
	cout<< "Please select the Game ID of the product you wish to remove from your shopping cart " << endl;
	cin >> id;
	cout << "===============================================================================" << endl << endl << endl;
	list<Game*>::iterator itorForCart = shoppingCart.begin();
	list<Game*>::iterator endForCart = shoppingCart.end();
	int count = 0;
	while(itorForCart!=endForCart)
	{
		if((*itorForCart)->GetID() == id)
		{
			count++;
			break;
		}
		itorForCart++;
	}
	if(count == 0)
		cout << "Unable to remove from cart due to wrong game ID" << endl;
	else{
		shoppingCart.erase(itorForCart);
		cout << "The game has been successfully removed from your shopping cart" << endl;
		cout << "===============================================================================" << endl << endl << endl;
	}
}

void Shopper::CheckOut()
{
	int totalCost = 0;
	list<Game*>::iterator itorForCart = shoppingCart.begin();
	list<Game*>::iterator endForCart = shoppingCart.end();
	while(itorForCart != endForCart)
	{
		totalCost += (*itorForCart)->GetCost();
		itorForCart++;
	}
	shoppingCart.clear();
	cout << "===============================================================================" << endl << endl << endl;
	cout << "The total cost is $" << totalCost << endl;
	cout << "Thanks for purchasing" << endl;
	cout << "===============================================================================" << endl << endl << endl;
}

void Shopper::ViewCart()
{
	shoppingCart.sort(compare_title);
	list<Game*>::iterator itorForCart = shoppingCart.begin();
	list<Game*>::iterator endForCart = shoppingCart.end();
	if(!(shoppingCart.empty())){
		cout << "===============================================================================" << endl << endl << endl;
		cout << "Current Games in your cart: " << endl;
		while(itorForCart != endForCart)
		{
			cout << **itorForCart << endl;
			itorForCart++;
		}
	} 
	else 
	{
		cout << "===============================================================================" << endl << endl << endl;
		cout << "Your shopping cart is empty" << endl;
	}
}

void Shopper::GameRating()
{
	int id;
	cout << "===============================================================================" << endl << endl << endl;
	cout << "Select the Game ID of the game you would like to rate" << endl;
	cin >> id;
	cout << "===============================================================================" << endl << endl << endl;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	int count = 0;
	while (itorForGame != endForGame)
	{
		if((*itorForGame)->GetID() == id)
		{
			count++;
			break;
		}
		itorForGame++;
	}
	if(count == 0)
		cout << "Unable to rate due to wrong game ID" << endl;
	else
	{
		int rating;
		cout<< "Please give a rating of the game" << endl;
		cin >> rating;
		if(rating == 1)
		{
			(*itorForGame)->SetRating(1,0,0,0,0);
			cout << "Game Successfully rated" << endl;
		} 
		else if(rating == 2)
		{
			(*itorForGame)->SetRating(0,1,0,0,0);
			cout << "Game Successfully rated" << endl;
		} 
		else if(rating == 3)
		{
			(*itorForGame)->SetRating(0,0,1,0,0);
			cout << "Game Successfully rated" << endl;
		} 
		else if(rating == 4)
		{
			(*itorForGame)->SetRating(0,0,0,1,0);
			cout << "Game Successfully rated" << endl;
		} 
		else if(rating == 5)
		{
			(*itorForGame)->SetRating(0,0,0,0,1);
			cout << "Game Successfully rated" << endl;
		} 
		else 
		{
			cout << "Invalid rate input" << endl << "Thus , exiting rating mode" << endl;
		}	
	}
}