#ifndef Shopper_H
#define Shopper_H
#include "Game.h"
#include "User.h"
#include "Rating.h"
#include <iostream>
#include <string>
#include <list>

using namespace std;

class Shopper : public User
{
public:
	
	//Pre-Condition: When the shopper exits
	//Post-Condition: Destroys Shopper

	~Shopper();
	
	//Pre-Condition: When the shopper is initialised
	//Post-Condition: Displays actions for the shopper to use

	void ShopperAction();
	
	//Pre-Condition: When the shopper wants to narrow down the list accordingly
	//Post-Condition: List of games is filtered and displayed accordingly

	void FilterBrowse();
	
	//Pre-Condition: When the shopper wants to add to cart
	//Post-Condition: Game is added to cart

	void AddCart();

	//Pre-Condition: When the shopper wants to remove a game from the cart
	//Post-Condition: Game is removed from the cart
	void RemoveCart();
	
	//Pre-Condition: When the shopper wants to leave and purchase the games
	//Post-Condition: Returns total price and displays the total cost

	void CheckOut();
	
	//Pre-Condition: When the user wants to rate a game
	//Post-Condition: Ratings are given to the specific game

	void GameRating();
	
	//Pre-Condition: When the user wants to view the games in shopping cart
	//Post-Condition: Displays the games in the shopping cart
	
	void ViewCart();

private:
	list<Game*> shoppingCart; 
};
#endif