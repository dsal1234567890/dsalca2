#include "User.h"

bool compare_title (const Game *first, const Game *second)
{
	return first-> GetName() < second->GetName();
}

User::User()
{
	ifstream inProduct;
	ifstream inRating;
	inRating.open("Rating.txt");
	inProduct.open("Product.txt");
	while(inProduct)
	{	
		Game *game = new Game();
		inProduct >> *game;
		if( game->GetName() == "*****" )
			delete game;
		else
			listOfGame.push_back(game);
	}
	while(inRating)
	{
		Rating *rating = new Rating();
		inRating >> *rating;
		if( rating->GetID() == -999)
			delete rating;
		else
			listOfRating.push_back(rating);
	}
	list<Rating*>::iterator itorForRating = listOfRating.begin();
	list<Rating*>::iterator endForRating = listOfRating.end();
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	while (itorForGame != endForGame)
	{
		while(itorForRating != endForRating)
		{

			if((*itorForRating)->GetID() == (*itorForGame)->GetID())
			{
				(*itorForGame)->SetRating((*itorForRating)->GetRatingList());
				itorForRating++;
			}
			else
				itorForRating++;
		}
		itorForGame++;
		itorForRating = listOfRating.begin();
	}
}

void User::PrintGameTitle()
{
	cout << "Current Games in store" << endl;
	cout << "===============================================================================" << endl << endl << endl;
	listOfGame.sort(compare_title);
    list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	while(itorForGame != endForGame)
	{
		cout << **itorForGame <<endl;
		itorForGame++;
	}
}

void User::PrintGameInfo()
{
	int id;
	cout << "===============================================================================" << endl << endl << endl;
	cout << "Please type a Game ID to view more info: " << endl;
	cin >> id;
	cout << "===============================================================================" << endl << endl << endl;
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	int count = 0;
	while (itorForGame != endForGame)
	{
		if((*itorForGame)->GetID() == id)
		{
			count++;
			break;
		}
		itorForGame++;	
	}
	if(count == 0)
	{
		cout << "Unable to view game info due to wrong game ID" << endl;
		cout << "===============================================================================" << endl << endl << endl;
	}
	else{
		cout << " * \t * \t * \t * \t * \t * \t * \t * \t * \t *" << endl;
		cout << " * \t * \t * \t * \t * \t * \t * \t * \t * \t *" << endl;
		cout << "Game ID:\t" << (*itorForGame)->GetID() << endl;
		cout << "Game Title:\t" <<  (*itorForGame)->GetName() << endl;
		cout << "Game Price:\t" << (*itorForGame)->GetCost() << endl;
		cout << "Developer:\t" << (*itorForGame)->GetDeveloper() << endl;
		cout << "Publisher:\t" << (*itorForGame)->GetPublisher() << endl;
		cout << "Description:\t" << (*itorForGame)->GetDescription() << endl;
		cout << "Genre:\t" << (*itorForGame)->GetGenre() << endl;
		cout << "Rating:\t" << (*itorForGame)->GetRating() << endl << endl;
		cout << " * \t * \t * \t * \t * \t * \t * \t * \t * \t *" << endl;
		cout << " * \t * \t * \t * \t * \t * \t * \t * \t * \t *" << endl;
	}
}

void User::PrintGameTitle(string input)
{
	if(input == "costasc")
	{
		listOfGame.sort(compare_cost_asc);
	}
	else if(input == "costdesc")
	{
		listOfGame.sort(compare_cost_desc);
	} 
	else if(input == "ratingasc")
	{
	    listOfGame.sort(compare_rating_asc);
	} else if(input == "ratingdesc")
	{
	    listOfGame.sort(compare_rating_desc);
	} else if(input == "idasc")
	{
	    listOfGame.sort(compare_id_asc);
	} else
	{
		listOfGame.sort(compare_title);
	}
	list<Game*>::iterator itorForGame = listOfGame.begin();
	list<Game*>::iterator endForGame = listOfGame.end();
	cout << "===============================================================================" << endl << endl << endl;
	cout << "===============================================================================" << endl << endl << endl;
	while(itorForGame != endForGame){
		cout << **itorForGame <<endl;
		itorForGame++;
	}
	cout << "===============================================================================" << endl << endl << endl;
	cout << "===============================================================================" << endl << endl << endl;

}

bool compare_cost_asc (const Game *first, const Game *second)
{
	return first->GetCost() < second->GetCost();
}

bool compare_cost_desc (const Game *first, const Game *second)
{
	return first->GetCost() > second->GetCost();
}

bool compare_rating_asc (const Game *first, const Game *second)
{
	return first->GetRating() < second->GetRating();
}
bool compare_rating_desc (const Game *first, const Game *second)
{
	return first->GetRating() > second->GetRating();
}

bool compare_id_asc (const Game *first, const Game *second)
{
	return first->GetID() < second->GetID();
}

