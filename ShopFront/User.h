#ifndef User_H
#define User_H

#include <iostream>
#include <list>
#include <fstream>
#include "Game.h"
#include "Rating.h"
using namespace std;

bool compare_title (const Game *first, const Game *second);
bool compare_cost_asc (const Game *first, const Game *second);
bool compare_cost_desc (const Game *first, const Game *second);
bool compare_rating_asc (const Game *first, const Game *second);
bool compare_rating_desc (const Game *first, const Game *second);
bool compare_id_asc (const Game *first, const Game *second);


class User
{
public:
	//Pre-Condition: When the User calls the constructor
	//Post-Condition: User is constructed

	User();

	//Pre-Condition: When the user needs to know what are the games available
	//Post-Condition: Shows the user the current games available

	virtual void PrintGameTitle();

	//Pre-Condition: When the user wants to sort the games
	//Post-Condition: Sorts the game according to user input

	void PrintGameTitle(string);

	//Pre-Condition: When the user wants to find out more information of a game
	//Post-Condition: More details is shown to the user

	void PrintGameInfo();

protected:
	//List of games inside the list
    list<Game*> listOfGame;
	//List of ratings of the game
	list<Rating*> listOfRating;
};
#endif